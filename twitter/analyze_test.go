package twitter_test

import (
	"testing"

	twitterGo "github.com/dghubble/go-twitter/twitter"
	"github.com/stretchr/testify/assert"
	"github.com/wipeinc/wipeinc/twitter"
)

func tweetWithMentions(mentions []string) twitterGo.Tweet {
	tweet := twitterGo.Tweet{}
	tweet.Entities = &twitterGo.Entities{}
	for _, mention := range mentions {
		entity := twitterGo.MentionEntity{IDStr: mention}
		tweet.Entities.UserMentions = append(tweet.Entities.UserMentions, entity)
	}
	return tweet
}

func tweetWithMention(mention string) twitterGo.Tweet {
	if mention == "" {
		mention = "mention"
	}
	tweet := twitterGo.Tweet{}
	tweet.Entities = &twitterGo.Entities{}
	entity := twitterGo.MentionEntity{IDStr: mention}
	tweet.Entities.UserMentions = append(tweet.Entities.UserMentions, entity)
	return tweet
}

var tweetWithHashtagTests = []struct {
	description string
	len         int
	in          []twitterGo.Tweet
	out         []twitter.Freq
}{
	{
		"tweets with no hashtag",
		0,
		[]twitterGo.Tweet{twitterGo.Tweet{}, twitterGo.Tweet{}},
		[]twitter.Freq{},
	},
}

var tweetWithMentionTests = []struct {
	description string
	len         int
	in          []twitterGo.Tweet
	out         []twitter.Freq
}{
	{
		"tweets with no mention",
		0,
		[]twitterGo.Tweet{twitterGo.Tweet{}, twitterGo.Tweet{}},
		[]twitter.Freq{},
	},
	{
		"tweets with one mention",
		0,
		[]twitterGo.Tweet{
			tweetWithMention(""),
			tweetWithMention(""),
			twitterGo.Tweet{},
		},
		[]twitter.Freq{twitter.Freq{Value: "mention", F: 2}},
	},
	{
		"with 3 mentions",
		0,
		[]twitterGo.Tweet{
			tweetWithMention("a"),
			tweetWithMention("a"),
			tweetWithMention("a"),
			tweetWithMention("b"),
			tweetWithMention("b"),
			tweetWithMention("c"),
			twitterGo.Tweet{},
		},
		[]twitter.Freq{
			twitter.Freq{Value: "a", F: 3},
			twitter.Freq{Value: "b", F: 2},
			twitter.Freq{Value: "c", F: 1},
		},
	},
	{
		"with 3 tweets, 2 on the same, mentions",
		0,
		[]twitterGo.Tweet{
			tweetWithMentions([]string{"a", "b"}),
			tweetWithMention("a"),
			tweetWithMention("a"),
			tweetWithMention("b"),
			tweetWithMention("c"),
			twitterGo.Tweet{},
		},
		[]twitter.Freq{
			twitter.Freq{Value: "a", F: 3},
			twitter.Freq{Value: "b", F: 2},
			twitter.Freq{Value: "c", F: 1},
		},
	},
	{
		"with 3 mentions and len 2",
		2,
		[]twitterGo.Tweet{
			tweetWithMention("a"),
			tweetWithMention("a"),
			tweetWithMention("a"),
			tweetWithMention("b"),
			tweetWithMention("b"),
			tweetWithMention("c"),
			twitterGo.Tweet{},
		},
		[]twitter.Freq{
			twitter.Freq{Value: "a", F: 3},
			twitter.Freq{Value: "b", F: 2},
		},
	},
}

func TestAnalyzeTweetWithHashtags(t *testing.T) {
	for _, tt := range tweetWithHashtagTests {
		stats := twitter.NewTweetStats()
		stats.AnalyzeTweets(tt.in)
		if !assert.Equal(t, tt.out, stats.TopHashtags(tt.len)) {
			t.Errorf("test: '%s' failed\n", tt.description)
		}
	}
}

func TestAnalyzeTweetWithMentions(t *testing.T) {
	for _, tt := range tweetWithMentionTests {
		stats := twitter.NewTweetStats()
		stats.AnalyzeTweets(tt.in)
		if !assert.Equal(t, tt.out, stats.TopMentions(tt.len)) {
			t.Errorf("test: '%s' failed\n", tt.description)
		}
	}
}

func TestAnalyzeTweetsIntegration(t *testing.T) {
	stats := twitter.NewTweetStats()
	stats.AnalyzeTweets(KimTimeline)

	const mostPopularTweetID = 997850510219620353
	if len(stats.MostPopularTweets) != 20 {
		t.Errorf("expected %d Most popular tweets got %d\n",
			20, len(stats.MostPopularTweets))
	}

	if stats.MostPopularTweets[0].ID != mostPopularTweetID {
		t.Errorf("expected most pouplar tweet: %d\n", mostPopularTweetID)
		t.Errorf("got: %d\n", stats.MostPopularTweets[0].ID)
	}
}
